ARG DEBIAN_RELEASE
FROM debian:$DEBIAN_RELEASE

RUN echo "deb http://ci-prod-controller.vm.liw.fi/debian unstable-ci main" > /etc/apt/sources.list.d/liw.fi.list
COPY liw.fi.asc /etc/apt/trusted.gpg.d/liw.fi.asc
RUN chmod a+r /etc/apt/trusted.gpg.d/liw.fi.asc

RUN apt-get update \
    && apt-get --assume-yes upgrade \
    && apt-get install --assume-yes summain subplot daemonize python3-requests \
        python3-yaml build-essential daemonize git libsqlite3-dev libssl-dev \
        moreutils pkg-config python3 python3-requests python3-yaml strace \
        subplot summain texlive-fonts-recommended texlive-latex-base \
        texlive-latex-recommended \
        curl
RUN apt-get clean \
    && apt-get autoremove

RUN useradd -m builder
USER builder
RUN curl --proto '=https' --tlsv1.2 -sSf --output /tmp/rustup.sh https://sh.rustup.rs \
    && chmod +x /tmp/rustup.sh \
    && /tmp/rustup.sh -y --default-toolchain 1.75.0 --profile minimal \
    && rm /tmp/rustup.sh

ENV PATH=/home/builder/.cargo/bin:$PATH
RUN rustup component add clippy rustfmt
RUN cargo install cargo-sweep
